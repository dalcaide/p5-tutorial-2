
//--------------------------------------------------------------
// Title:
// ------
// 4.Function-reading-flights-data
// 
// Description:
// ------------
//The aim of this exercise is to create a horizontal bar chart
// reading the file flights.csv
//
// What's your task?
// -----------------
// Based on the exercise 3. Function-bar-chart please modify it to 
// complete the code
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '4.Function-reading-flights-data/evaluation.png'
// 
//--------------------------------------------------------------

var table;

function preload() {
  /* TODO: Read the csv file flights.csv */
  table = ...
}

function setup() {
  // Canvas dimension (width and height)
  /* --> TODO: Define width and height */
  var width = ..., 
      height = ...;
  createCanvas(width, height);
  noLoop();
  /* TODO: Use the function getColumn() from the class table to extract
  the column 'distance'. Reference: https://p5js.org/reference/#/p5.Table */
  data = countQuantiles(...)
}


function draw () {
    barchart(data);
}


function barchart(data) {
  // We define the space for the label of our data
  var margin = width * 0.2;
  
    // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
    /* --> TODO: Define the size of the bar and the margin */
  var barHeight =  ..., 
      barMargin = ...;

  // We go over all data points
  /* --> TODO: Complete a for loop to go over the data */
  for(...) {
    
    push();
    /* --> TODO: Complete the translate function to jump to the top left of the bar
     and write the label of the data element */
    translate(...);
    fill(0);
    text(data[i].name, margin * .25, barHeight/2 );
    pop();
    
    push();
    // We jump to the bottom left corner of the bar
    /* --> TODO: Complete the translate function to jump to the top left of the bar */
    translate(margin, ...); 
    // We draw the bar
    fill('steelblue'); noStroke();
    /* --> TODO: Draw a horizontal bar based on the value of the data but remember 
    that the width of the bar starts on 0 value and ends on position width-margin */
    var barWidth = ...
    rect(...);
    // We draw the value
    fill(0);
    text(data[i].value + " %", (width-margin) * .25, barHeight/2 );
    pop();
  }
}


function countQuantiles (arr) {
  var quantiles = [
    {"name":"[0:299)", "min":0, "max": 299, 'value': 0},
    {"name":"[299:473)", "min":299, "max": 473, 'value': 0},
    {"name":"[473:666)", "min":473, "max": 666, 'value': 0},
    {"name":"[666:875)", "min":666, "max": 875, 'value': 0},
    {"name":"[875:1131)", "min":875, "max": 1131, 'value': 0}, 
    {"name":"[1131:1445)", "min":1131, "max": 1445, 'value': 0},  
    {"name":"[1445:1870)", "min":1445, "max": 1870, 'value': 0}, 
    {"name":"[1870:2669)", "min":1870, "max": 2669, 'value': 0},
    {"name":"[2669:4397)", "min":2669, "max": 4397, 'value': 0}, 
    {"name":"[>4397)", "min":4397, "max": 99999999, 'value': 0}
  ]
  
    arr.sort();
    for (var j = 0; j < quantiles.length; j++) {
      for ( var i = 0; i < arr.length; i++ ) {
        if ( quantiles[j].min < +arr[i] && +arr[i] < quantiles[j].max) {
          quantiles[j].value++;
        }
      }
    }
    
    for (j = 0; j < quantiles.length; j++) {
      quantiles[j].value = Math.round(1000 * quantiles[j].value / arr.length) / 10;
    }
    return quantiles;
}

