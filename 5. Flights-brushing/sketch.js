
//--------------------------------------------------------------
// Title:
// ------
// 5.Flights-brushing
// 
// Description:
// ------------
//The aim of this exercise is visualize departure coordinates of 
// flights dataset and implement the brushing interaction to highlight
// brushed elements.
//  
//
// What's your task?
// -----------------
// Based on the exercise the set of exercise performed last week, complete
// the following code.
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '5.Flights-brushing/evaluation.png'
// 
//--------------------------------------------------------------



var table, rows;

// Start position for brushing interaction
var startSelectX, startSelectY;
// Off-screen graphics buffer to save all the airports
var airportsImage;

/* TODO: Create the flight class that saves the variables:
  - distance: original distance variable
  - from_long: original longitude form departure airport 
  - from_lat: original latitude from departure airport
  - departureX: mapped longitude position using the width of the canvas as output range
  - departureY: mapped latitude position using the height of the canvas as output range */
var flight = function(row){
  this.distance = ...;
  this.from_long = ...;
  this.from_lat = ...;
  this.departureX = map(...);
  this.departureY = map(...);
}


function preload() {
  /* TODO: Read the csv file flights.csv */
  table = ...
  rows = table.getRows();
}

function setup() {
  createCanvas(900,400);
  noLoop();
  
  // Drawing all airports and saving them into airportsImage variable
  airportsImage = createGraphics(width,height);
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    /* TODO: Draw the coordinates of departure airports using 'thisFlight' object.
        We want to save all these drawings inside the airportsImage; therefore, all statements
        must be refered to airportsImage, for example:
        airportsImage.fill(...)
        airportsImage.rect(...)*/
    
  }
  
}

function draw() {
  image(airportsImage, 0, 0);
}

// Mouse interaction
function mousePressed() {
  startSelectX = mouseX;
  startSelectY = mouseY;
}

function mouseDragged() {
  clear()
  redraw();
  fill(0,0,0,50);
  rect(startSelectX, startSelectY, mouseX-startSelectX, mouseY-startSelectY);
}

function mouseReleased() {
  clear();
  redraw();
  
  stopSelectX = mouseX;
  stopSelectY = mouseY;

  startX = min(startSelectX, stopSelectX);
  stopX =  max(startSelectX, stopSelectX);
  startY = min(startSelectY, stopSelectY);
  stopY =  max(startSelectY, stopSelectY);

  selectDataPoints(startX, startY, stopX, stopY);
}


function selectDataPoints(startX, startY, stopX, stopY) {
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    /* TODO: Complete the function to highlight only the brushed flights */
    ...
  }
}