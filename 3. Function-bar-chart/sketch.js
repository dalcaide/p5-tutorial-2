
//--------------------------------------------------------------
// Title:
// ------
// 3. Function-bar-chart
// 
// Description:
// ------------
// The aim of this exercise is to create a horizontal bar chart using 
// the variable 'data' that is a javascript object that has seven elements. 
// Each element is composed of a string (name) and a number (value).
//
// What's your task?
// -----------------
// Based on the exercise 2. Horizontal-bar-chart please modify it to 
// complete the code
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '3.Function-bar-chart/evaluation.png'
// 
//--------------------------------------------------------------

// The data to visualize
var data = [{'name':'France',  'value': 212}, 
            {'name':'Spain',   'value': 105},
            {'name':'Belgium', 'value': 54},
            {'name':'Germany', 'value': 158},
            {'name': 'Italy',  'value': 98},
            {'name':'Grece',   'value': 31}];


function setup() {
  // Canvas dimension (width and height)
  /* --> TODO: Define width and height */
  var width = ..., 
      height = ...;
  createCanvas(width, height);
  noLoop();
}

function draw () {
    barchart(data);
}


function barchart(data) {
  
  // We define the space for the label of our data
  var margin = width * 0.2;
  // We compute the max value of our data
  var maxData = maxValueFromData(data);
  
    // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
    /* --> TODO: Define the size of the bar and the margin */
  var barHeight =  ..., 
      barMargin = ...;

  // We go over all data points
  /* --> TODO: Complete a for loop to go over the data */
  for(...) {
    
    push();
     /* --> TODO: Complete the translate function to jump to the top left of the bar
     and write the label of the data element */
    translate(...);
    fill(0);
    text(data[i].name, margin * .25, barHeight/2 );
    pop();
    
    push();
    // We jump to the bottom left corner of the bar
    /* --> TODO: Complete the translate function to jump to the top left of the bar */
    translate(margin, ...); 
    // We draw the bar
    fill('steelblue'); noStroke();
    /* --> TODO: Draw a horizontal bar based on the value of the data but remember 
    that the width of the bar starts on margin value and ends on width variable */
    var barWidth = ...
    rect(...);
    // We draw the value
    fill('#FAFAFA');
    text(data[i].value, width * .025, barHeight/2 );
    pop();
  }
}

function maxValueFromData(data){
  values = [];
  data.forEach(function(d){ values.push(d.value) });
  return max(values);
}

