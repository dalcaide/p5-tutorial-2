
//--------------------------------------------------------------
// Title:
// ------
// 2. Horizontal-bar-chart
// 
// Description:
// ------------
//The aim of this exercise is to create a horizontal bar chart
// using the variable 'data' that is an array with seven values
//
// What's your task?
// -----------------
// Based on the exercise 1. Simple-bar-chart please modify it to 
// create a horizontal bar chart
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '2.Horizontal-bar-chart/evaluation.png'
// 
//--------------------------------------------------------------

function setup() {

  // The data to visualize
  var data = [105, 212, 158, 31, 98, 54],
      maxData = max(data);
  
  // Canvas dimension (width and height)
   /* --> TODO: Define width and height */
  var width = ..., 
      height = ...;
  createCanvas(width, height);
  
  // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    // barMargin is the space between two bars
    /* --> TODO: Define the size of the bar and the margin */
  var barHeight =  ..., 
      barMargin =  ...;
  
  // We go over all data points
  /* --> TODO: Complete a for loop to go over the data */
  for(...) {
    
    push();
    // We jump to the bottom left corner of the bar
    /* --> TODO: Complete the translate function to jump to the top left of the bar */
    translate(...); 
    // We draw the bar
    fill('steelblue'); noStroke();
    /* --> TODO: Draw a horizontal bar based on the value of the data */
    var barWidth = ...
    rect(...);
    // We draw the value
    fill('#FAFAFA');
    text(data[i], width * .025, barHeight/2 );
    pop();
  }
}