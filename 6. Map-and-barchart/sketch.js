
//--------------------------------------------------------------
// Title:
// ------
// 6.Map-and-barchart
// 
// Description:
// ------------
//The aim of this exercise to combine the exercise 4. and 5. to create
// an interactive map that uses the brushing interaction to select  data for 
// the bar chart
//  
// What's your task?
// -----------------
// Based on the last two exercises complete the following code.
//
// Evaluation
// ----------
// The resulting output should be similar to the image that you
// you can find in '6.Map-and-barchart/evaluation.png'
// 
//--------------------------------------------------------------

var table, rows;

// Off-screen graphics buffer to save all the airports
var airportsImage;

// Selected flights to be visualized by the barchart
var selectedFlights = [];

// Start position for brushing interaction
var startSelectX, startSelectY;

// Distribution of the space for visualization
var heightCanvas = 400;
var widthCanvas =  1200;
var widthMap = widthCanvas * .8;
var widthBar = widthCanvas * .4;

/* TODO: Create the flight class that saves the variables:
  - distance: original distance variable
  - from_long: original longitude form departure airport 
  - from_lat: original latitude from departure airport
  - departureX: mapped longitude position using the width of the map (widthMap) as output range
  - departureY: mapped latitude position using the height of the map (widthBar) as output range */
var flight = function(row){
  this.distance = ...;
  this.from_long = ...;
  this.from_lat = ...;
  this.departureX = map(...);
  this.departureY = map(...);
}

function preload() {
  /* TODO: Read the csv file flights.csv */
  table = ...
  rows = table.getRows();
}

function setup() {
  createCanvas(widthCanvas, heightCanvas);
  noLoop();
  
  // Drawing all airports and saving them into airportsImage variable
  airportsImage = createGraphics(width,height);
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    /* TODO: Draw the coordinates of departure airports using 'thisFlight' object.
        We want to save all these drawings inside the airportsImage; therefore, all statements
        must be refered to airportsImage, for example:
        airportsImage.fill(...)
        airportsImage.rect(...)*/
    
  }
}

function draw() {
  image(airportsImage, 0, 0);
}

function mousePressed() {
  startSelectX = mouseX;
  startSelectY = mouseY;
}

function mouseDragged() {
  clear()
  redraw();
  fill(0,0,0,50);
  rect(startSelectX, startSelectY, mouseX-startSelectX, mouseY-startSelectY);
}

function mouseReleased() {
  clear();
  redraw();
  
  stopSelectX = mouseX;
  stopSelectY = mouseY;

  startX = min(startSelectX, stopSelectX);
  stopX =  max(startSelectX, stopSelectX);
  startY = min(startSelectY, stopSelectY);
  stopY =  max(startSelectY, stopSelectY);

  selectDataPoints(startX, startY, stopX, stopY);
  drawBarchart();
}

function selectDataPoints(startX, startY, stopX, stopY) {
  for (var r = 0; r < rows.length; r++) {
    var thisFlight = new flight(rows[r]);
    /* TODO: Complete the function to highlight only the brushed flights */
    ...
  }
}

function drawBarchart(){
  data = countQuantiles(selectedFlights);
  /* TODO: Call the bar chart function and draw the graph after map.
  You can use the translate function to position the bar chart on the desired positon */
  
  ...
}


function barchart(data) {
  // We define the space for the label of our data
  var margin = widthBar * 0.2;
  
    // Defining the bars of our graphic 
    // We define two variables here (barHeight and barMargin).
    /* --> TODO: Define the size of the bar and the margin */
  var barHeight =  ...
      barMargin = ...

  // We go over all data points
  /* --> TODO: Complete a for loop to go over the data */
    
    push();
    /* --> TODO: Complete the translate function to jump to the top left of the bar
     and write the label of the data element */
    translate(...);
    fill(0);
    text(data[i].name, margin * .25, barHeight/2 );
    pop();
    
    push();
    // We jump to the bottom left corner of the bar
    /* --> TODO: Complete the translate function to jump to the top left of the bar */
    translate(margin, ...); 
    // We draw the bar
    fill('steelblue'); noStroke();
    /* --> TODO: Draw a horizontal bar based on the value of the data but remember 
    that the width of the bar starts on margin value and ends on position width - margin */
    var barWidth = ...
    rect(...);
    // We draw the value
    fill(0);
    text(data[i].value + " %", (widthBar - margin) * .25, barHeight/2 );
    pop();
  }
}

function countQuantiles (arr) {
  var quantiles = [
    {"name":"[0:299)", "min":0, "max": 299, 'value': 0},
    {"name":"[299:473)", "min":299, "max": 473, 'value': 0},
    {"name":"[473:666)", "min":473, "max": 666, 'value': 0},
    {"name":"[666:875)", "min":666, "max": 875, 'value': 0},
    {"name":"[875:1131)", "min":875, "max": 1131, 'value': 0}, 
    {"name":"[1131:1445)", "min":1131, "max": 1445, 'value': 0},  
    {"name":"[1445:1870)", "min":1445, "max": 1870, 'value': 0}, 
    {"name":"[1870:2669)", "min":1870, "max": 2669, 'value': 0},
    {"name":"[2669:4397)", "min":2669, "max": 4397, 'value': 0}, 
    {"name":"[>4397)", "min":4397, "max": 99999999, 'value': 0}
  ]
  
    arr.sort();
    for (var j = 0; j < quantiles.length; j++) {
      for ( var i = 0; i < arr.length; i++ ) {
        if ( quantiles[j].min < +arr[i] && +arr[i] < quantiles[j].max) {
          quantiles[j].value++;
        }
      }
    }
    
    for (j = 0; j < quantiles.length; j++) {
      quantiles[j].value = Math.round(1000 * quantiles[j].value / arr.length) / 10;
    }
    return quantiles;
}
